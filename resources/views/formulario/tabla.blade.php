<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Examen 1</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .imagen a img
            {
                width: 70%;
                border-radius: 50%;
                border: 2px solid #000;
                margin-left: 120%;
                margin-top: -5%;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
            <div class="title m-b-md">
                    Jesus Reynaldo Yujra Charca
                </div>
                <div class="imagen col-xs-4 col-md-4">
                    <a href="http://192.168.101.110/redesdos/index.html"><img src="./img/gato.jpg" class="img-responsive"/></a>
                </div>
                <br>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">Nombres</th>
                        <th scope="col">Telefono</th>
                        <th scope="col">Correo</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">Maria Sanchez</th>
                        <th scope="row">71598748</th>
                        <th scope="row">mary@gmail.com</th>
                        </tr>
                        <tr>
                        <th scope="row">Jorge Mazoquey</th>
                        <th scope="row">62596470</th>
                        <th scope="row">jorgito@gmail.com</th>
                        </tr>
                    </tbody>
                </table>
                <button type="button" class="btn btn-info btn-lg">Entrar</button> 
                <button type="button" class="btn btn-secondary btn-lg">Salir</button>                               
            </div>
        </div>
    </body>
</html>